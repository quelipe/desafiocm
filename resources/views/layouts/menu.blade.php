<nav class="navbar navbar-expand-lg navbar-light bg-light menu">
  	<div class="container-fluid">
        <a class="navbar-brand" href="/adm/inicio">
        	<img src="https://consultmidia.it/wp-content/uploads/2020/03/logo-oficial-4_7b80b499131f90466212f091336eca30.png" alt="" width="150" height="" class="d-inline-block align-text-top">
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
          	<span class="navbar-toggler-icon"></span>
        </button>
		<div class="collapse navbar-collapse" id="navbarNavDropdown">
      		<ul class="navbar-nav">
      			<li class="nav-item">
          			<a class="nav-link active" aria-current="page" href="/adm/inicio">Home</a>
        		</li>
      			@can('admin')
        		<li class="nav-item">
          			<a class="nav-link active" aria-current="page" href="{{route('usuario.lista')}}">Usuários</a>
        		</li>
        		@endcan
        		<li class="nav-item">
          			<a class="nav-link" href="{{route('influenciador.lista')}}">Influenciadores</a>
        		</li>
        		<li class="nav-item dropdown">
          			<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            			<strong>{{ Auth::user()->name}}</strong>
          			</a>
          			<ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            			<li><a class="dropdown-item" href="{{route('logout')}}">Sair</a></li>
          			</ul>
        		</li>
     		</ul>
    	</div>
  	</div>
</nav>

