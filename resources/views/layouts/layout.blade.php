<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Desafio Consult Mídia</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.0/css/all.min.css" integrity="sha512-BnbUDfEUfV0Slx6TunuB042k9tuKe3xrD6q4mg5Ed72LTgzDIcLPxg6yI2gcMFRyomt+yJJxE+zJwNmxki6/RA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap5.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/custom.css?rand=') }}{{rand()}}">
    
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.11.5/js/dataTables.bootstrap5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.1.0/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
    <script src="{{ asset('js/custom.js?rand=') }}{{rand()}}"></script>
    
</head>
    <body>
    	<div class="container-fluid menu">
       		@include('layouts/menu')
     	</div>
		<div class="container mt-3 mb-5">
            @yield('conteudo')
    	</div>
		<footer class="footer">
        	<div class="container-fluid">
        		<div class="row">
        			<div class="col-md-12 text-center" id="rodape">
        				&copy; {{date("Y")}} - Edimilsopn Quelipe - <a href="https://www.quelipe.com.br">www.quelipe.com.br</a>
        			</div>
        		</div>
        	</div>
        </footer>
        <!-- Modal alertas-->
        <div class="modal fade" id="modalAlerta" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                    	<h5 class="modal-title" id="modalTitleAlerta">Alerta</h5>
                    	<button type="button" class="btn-close btn-close-white fechar" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body text-center" id="modalBodyAlerta"></div>
                    <div class="modal-footer">
                    	<button type="button" id="btn-modal-ok" class="btn btn-sm btn-primary fechar btn-modal-msg alerta" data-bs-dismiss="modal">OK</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal para confirmação-->
        <div class="modal fade" id="modalConfirm" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                    	<h5 class="modal-title" id="modalTitleConfirm">Confirmação</h5>
                    	<button type="button" class="btn-close btn-close-white fechar" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body text-center" id="modalBodyConfirm"></div>
                    <div class="modal-footer">
                    	<button type="button" id="btn-modal-sim" class="btn btn-sm btn-primary fechar btn-modal-msg" data-bs-dismiss="modal">Sim</button>
                    	<button type="button" id="btn-modal-nao" class="btn btn-sm btn-secondary fechar btn-modal-msg" data-bs-dismiss="modal">Não</button>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>