<hr>
@if ($errors->any())
    <div class="alert alert-danger alert-dismissible">
        <div class="font-medium text-red-600">
            Opa! Algo deu errado.
        </div>

        <ul class="mt-3 list-disc list-inside text-sm text-red-600">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
@endif