@extends('layouts/layout')
@section('conteudo')

<div class="container-fluid"> 
	<div class="row">
		<div class="col-10"><h3>{{isset($usuario) ? 'Alterar Influenciador ' : 'Influenciador ' }} Digital</h3></div>
	</div>

	@include('layouts/msg')
	<form id="form-usuario" action="{{isset($usuario) ? route('influenciador.alterar') : route('influenciador.cadastrar') }}" method="post">
	 	@csrf
	 	@if(isset($usuario))
	 	<input type="hidden" name="id" value="{{$usuario->id}}">
	 	@endif
		<div class="mb-3 row">
			<div class="mb-3 col-md-3">
        		<label>CPF</label>
        		<input type="text" maxlength="14" {{!isset($usuario) ? 'required' : 'readonly'}} required class="form-control cpf" name="cpf" id="cpf" value="{{$usuario->cpf ?? old('cpf')}}">
    		</div>
			<div class="mb-3 col-md-5">
        		<label>Nome</label>
        		<input type="text" required class="form-control" name="name" id="name" value="{{$usuario->name ?? old('name')}}">
    		</div>
			<div class="mb-3 col-md-4">
        		<label>E-mail</label>
        		<input type="text" {{!isset($usuario) ? 'required' : 'readonly'}} class="form-control" name="email" id="email" value="{{$usuario->email ?? old('email')}}">
    		</div>
    		<div class="mb-3 col-md-4">
        		<label>Nascimento</label>
        		<input type="date" maxlength="14" required class="form-control" name="nascimento" id="nascimento" value="{{$usuario->nascimento ?? old('nascimento')}}">
    		</div>
    		
    		<div class="mb-3 col-md-4">
        		<label>Senha</label>
        		<input type="password" {{!isset($usuario) ? 'required' : ''}} class="form-control" name="password" id="password" value="">
    		</div>
    		<div class="mb-3 col-md-4">
        		<label>Confirmar Senha</label>
        		<input type="password" {{!isset($usuario) ? 'required' : ''}} class="form-control" name="password_confirmation" id="password_confirmation" value="">
    		</div>
    		@if(isset($usuario))
    	 	<input type="hidden" name="rede_id[]" value="{{$usuario->influenciadores[0]->id ?? ''}}">
    	 	<input type="hidden" name="rede_id[]" value="{{$usuario->influenciadores[1]->id ?? ''}}">
    	 	<input type="hidden" name="rede_id[]" value="{{$usuario->influenciadores[2]->id ?? ''}}">
    	 	@endif
    		<div class="mb-3 col-md-4">
        		<label>Usuário Facebook</label>
        		<input type="hidden" name="rede[]" id="rede[]" value="Facebook">
        		<input type="text" autocomplete="0" class="form-control" name="usuario[]" id="usuario[]" value="{{$usuario->influenciadores[0]->usuario ?? old('usuario[0]')}}">
    		</div>
    		<div class="mb-3 col-md-4">
        		<label>Usuário Instagram</label>
        		<input type="hidden" name="rede[]" id="rede[]" value="Instagram">
        		<input type="text" autocomplete="0" class="form-control" name="usuario[]" id="usuario[]" value="{{$usuario->influenciadores[1]->usuario ?? old('usuario[1]')}}">
    		</div>
    		<div class="mb-3 col-md-4">
        		<label>Usuário Tik Tok</label>
        		<input type="hidden" name="rede[]" id="rede[]" value="Tik Tok">
        		<input type="text" autocomplete="0" class="form-control" name="usuario[]" id="usuario[]" value="{{$usuario->influenciadores[2]->usuario ?? old('usuario[2]')}}">
    		</div>
        	
    	</div>
    	
 
		<div class="btn-tela">
        	<button type="submit" class="btn btn-primary"> Salvar</button>
			<button type="button" class="btn btn-secondary" onclick="document.location='/influenciador'"> Cancelar</button>
		</div>
	</form>
</div>
@stop

