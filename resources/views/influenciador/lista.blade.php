@extends('layouts/layout')
@section('conteudo')

<div class="container-fluid">
	<div class="row">
		<div class="col-10"><h3>Influenciadores digital</h3></div>
		<div class="col-2"><button class="btn btn-primary" onclick="document.location='{{route('influenciador.cadastro')}}'" style="float: right;">Novo</button></div>
	</div>
	@include('layouts/msg')
    <table class="table table-hover table-sm dataTable">
        <thead>
            <tr>
            	@foreach($array['colunas'] as $dado)
            	<th>{{$dado}}</th>
            	@endforeach
            	<th style="text-align: center;width: 150px">Ações</th>
            </tr>
        </thead>
        <tbody>
        @foreach($array['data'] as $dados)
        	<tr>
        		<td>{{$dados->user->name}}</td>
        		<td>{{$dados->user->email}}</td>
        		<td style="text-align: center">
        			<div class="acoes">
                		<a class='editar' href="{{route('influenciador.editar',['id'=>$dados->user->id])}}"><i class='fas fa-edit'></i></a>
                		<a class='visualizar' href="{{route('influenciador.visualizar',['id'=>$dados->user->id])}}"><i class='fas fa-search-plus'></i></a>
                		<span class='excluir' onclick="excluir('/influenciador/excluir/{{$dados->user->id}}')"><i class='fas fa-times-circle'></i></span>
        			</div>
        		</td>
        	</tr>
        @endforeach
        </tbody>
    </table>
</div>
@stop