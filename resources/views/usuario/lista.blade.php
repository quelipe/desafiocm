@extends('layouts/layout')
@section('conteudo')
@can('admin')
<div class="container-fluid">
	<div class="row">
		<div class="col-10"><h3>Usuários</h3></div>
		<div class="col-2"><button class="btn btn-primary" onclick="document.location='{{route('usuario.cadastro')}}'" style="float: right;">Novo</button></div>
	</div>
	<hr>
    <table class="table table-hover dataTable table-sm">
        <thead>
            <tr>
            	@foreach($array['colunas'] as $dado)
            	<th>{{$dado}}</th>
            	@endforeach
            	<th style="text-align: center;width: 150px">Ações</th>
            </tr>
        </thead>
        <tbody>
        @foreach($array['data'] as $dados)
        	<tr>
        		<td>{{$dados->name}}</td>
        		<td>{{$dados->email}}</td>
        		<td style="text-align: center">
        			<div class="acoes">
                		<span class='excluir' onclick="excluir('/usuario/excluir/{{$dados->id}}')"><i class='fas fa-times-circle'></i></span>
        			</div>
        		</td>
        	</tr>
        @endforeach
        </tbody>
    </table>
</div>
@endcan
@stop