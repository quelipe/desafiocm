@extends('layouts/layout')
@section('conteudo')

<div class="container-fluid"> 
	<div class="row">
		<div class="col-10"><h3>Usuários</h3></div>
	</div>

	@include('layouts/msg')
	<form id="form-usuario" action="{{ route('usuario.cadastrar') }}" method="post">
	 	@csrf
		<div class="mb-3 row">
    		
			<div class="mb-3 col-md-6">
        		<label>Nome</label>
        		<input type="text" required class="form-control" name="name" id="name" value="{{old('name')}}">
    		</div>
			<div class="mb-3 col-md-6">
        		<label>E-mail</label>
        		<input type="text" required class="form-control" name="email" id="email" value="{{old('email')}}">
    		</div>
    		<div class="mb-3 col-md-4">
        		<label>Senha</label>
        		<input type="password" required class="form-control" name="password" id="password" value="">
    		</div>
    		<div class="mb-3 col-md-4">
        		<label>Confirmar Senha</label>
        		<input type="password" required class="form-control" name="password_confirmation" id="password_confirmation" value="">
    		</div>
        	
    	</div>
    	
 
		<div class="btn-tela">
        	<button type="submit" class="btn btn-primary"> Salvar</button>
			<button type="button" class="btn btn-secondary" onclick="document.location='/usuario'"> Cancelar</button>
		</div>
	</form>
</div>
@stop

