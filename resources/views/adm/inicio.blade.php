@extends('layouts/layout')
@section('conteudo')


@php
$rede = Auth::user()->influenciadores()->firstWhere('rede_social','Instagram');
$usuario = Auth::user();
@endphp
<div class="container-fluid">
	<div class="row">
		<div class="col-10"><h3>Seja bem vindo, {{ Auth::user()->name}}</h3></div>
	</div>
	<hr>
	@if($usuario->cpf)
	<div class="row mb-3">
		<div class="col-3 text-center" style="border: 1px solid #c8c9ca;padding-top: 10px">
			@if($rede && $rede->url_foto != "")
			<img src="{{'data:image/jpg;base64,'.base64_encode(file_get_contents($rede->url_foto))}}" width="100%">
			<div>Seguidores Instagram: <strong>{{$rede->seguidores}}</strong></div>
			@endif
		</div>
		<div class="col-9">
		
        	<div class="mb-3 row">
        		<div class="mb-3 col-md-4">
            		<label class="label-view">CPF:</label>
            		<div>{{ $usuario->cpf }}</div>
        		</div>
        		<div class="mb-3 col-md-4">
            		<label class="label-view">E-mail:</label>
            		<div>{{ $usuario->email }}</div>
        		</div>
        		<div class="mb-3 col-md-4">
            		<label class="label-view">Nascimento:</label>
            		<div>{{ \Carbon\Carbon::parse($usuario->nascimento)->format('d/m/Y') }}</div>
        		</div>
        	</div>
        	<div class="mb-3 row">
        		@foreach($usuario->influenciadores as $dados)
        		<div class="mb-3 col-md-4">
            		<label class="label-view">Usuário {{$dados->rede_social}}:</label>
            		<div>{{ $dados->usuario }}</div>
        		</div>
        		@endforeach
        		
        	</div>
		</div>
	</div>
	@endif
</div>
@stop
