$(document).ready(function() {
    $('.dataTable').DataTable({
        responsive: true,
        language: {url: "/js/Portuguese-Brasil.json"},
        order: []
	});
	
	$(".cpf").mask("000.000.000-00");
	
	$(".modal").modal({
        show: false,
        backdrop: 'static',
		keyboard: false
    });

	$("#modalAlerta .fechar, #modalErro .fechar").click(function () {
		eval(funcao);
		funcao = "";
	});
	
	$("#btn-modal-sim").click(function () {
		eval(funcao);
		funcao = "";
	});
} );
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
var funcao = "";

function excluir(rota) {
	confirmModal("Deseja realmente excluir o registro?");
	funcao = "excluirAjax('"+rota+"')";
}

function excluirAjax(url) {
	
	$.ajax({
		type: 'POST',
		dataType: 'json',
		url: url,
		contentType: false,
        processData: false,
		success: function(obj){
			if(obj.funcao) {
				funcao = obj.funcao;
			}
			alertaModal(obj.msg);
		},
		beforeSend: function(){
	    },
		complete: function () {
		},
		error: function(e){
			console.log(e);
		}
	});
	
}
function alertaModal(texto) {
	$('#modalBodyAlerta').html("");
	$('#modalBodyAlerta').html(texto);
	$("#modalAlerta").modal('show');
	
	$('#modalAlerta').on('shown.bs.modal', function () {
	  	$('.alerta').trigger('focus')
	})
	
}

function confirmModal(texto) {
	$('#modalBodyConfirm').html("");
	$('#modalBodyConfirm').html(texto);
	$("#modalConfirm").modal('show');
}