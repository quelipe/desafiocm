<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UsuarioController;
use App\Http\Controllers\InfluenciadorController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Route::middleware(['auth'])->group(function() {
    Route::group(['prefix' => 'adm'], function() {
        Route::view('/inicio', 'adm.inicio');
        
    });
    Route::group(['prefix' => 'usuario'], function() {
        Route::get('/',[UsuarioController::class, 'list'])->name('usuario.lista');
        Route::get('/cadastro',[UsuarioController::class, 'new'])->name('usuario.cadastro');
        Route::post('/cadastrar',[UsuarioController::class, 'store'])->name('usuario.cadastrar');
        Route::post('/excluir/{id}',[UsuarioController::class, 'delete'])->name('usuario.excluir');
        
    });
    Route::group(['prefix' => 'influenciador'], function() {
        Route::get('/',[InfluenciadorController::class, 'list'])->name('influenciador.lista');
        Route::get('/cadastro',[InfluenciadorController::class, 'new'])->name('influenciador.cadastro');
        Route::post('/cadastrar',[InfluenciadorController::class, 'store'])->name('influenciador.cadastrar');
        Route::get('/visualizar/{id}',[InfluenciadorController::class, 'show'])->name('influenciador.visualizar');
        
        Route::get('/editar/{id}',[InfluenciadorController::class, 'edit'])->name('influenciador.editar');
        Route::post('/excluir/{id}',[InfluenciadorController::class, 'delete'])->name('influenciador.excluir');
        Route::post('/alterar',[InfluenciadorController::class, 'update'])->name('influenciador.alterar');
        
    });
});

 require __DIR__.'/auth.php';
