<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## Instalação
Após o clone do projeto, executar os passos abaixo:<br>
1 - Alterar seu arquivo **.env** com suas credenciais de banco.<br>
2 - Executar o comando: **composer install --optimize-autoloader --no-dev**<br>
3 - Executar o comando: **php artisan config:cache**<br>
4 - Executar o comando: **php artisan route:cache**<br>
5 - Executar as migrations: **php artisan migrate**<br>
6 - Criar a permissão de administrador: **php artisan permission:create-permission admin**<br>
7 - Criar a permissão de influence: **php artisan permission:create-permission influence**<br>
8 - Iniciar seu servidor: **php artisan serve**<br>
9 - Acessar: **http://seu_servidor/register** para criar um usuário para o primeiro acesso ao sistema, deixei essa funcionalidade apenas para testes, pois em ambiente real seria uma carga no banco com um susuário inicial.<br>
