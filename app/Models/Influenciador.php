<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Influenciador extends Model
{
    use HasFactory;
    
    protected $table      = 'influencers';
    protected $primaryKey = "id";
    
    protected $fillable = [
        'rede_social',
        'usuario',
        'seguidores',
        'url_foto',
        'user_id',
    ];
    
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id','id');
    }
}
