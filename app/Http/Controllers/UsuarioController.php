<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;
use Response;

class UsuarioController extends Controller
{
    //
    
    public function list()
    {
        $data = User::whereNull('cpf')->orderBy('id','desc')->get();
        $array['colunas'] = ['Nome','E-mail'];
        $array['data']    = $data;
        return view('usuario.lista', ['array'=>$array]);
    }
    
    public function new()
    {
        return view('usuario.cadastro');
    }
    
    public function store(Request $request)
    {
        $request->validate([
            'name'     => ['required', 'string', 'max:255'],
            'email'    => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
        ]);
        
        User::create([
            'name'     => $request->name,
            'email'    => $request->email,
            'password' => Hash::make($request->password),
        ])->givePermissionTo('admin');
        
        return redirect('/usuario');
    }
    
    public function delete(Request $request)
    {        
        try {
            
            $user = User::where('id',$request->id)->firstOrFail();
            $user->delete();
            return Response::json([
                'success' => true,
                'funcao'  => "document.location = '/usuario'",
                'msg'     => "Usuário excluído com sucesso.",
            ]);
        } catch (\Exception $e) {
            return Response::json([
                'success' => false,
                'msg'     => "Erro ao excluir o usuário.",
            ]);
        }
    }
}
