<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Influenciador;
use Illuminate\Validation\Rules;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Response;

class InfluenciadorController extends Controller
{
    //
    private $rules = [
        'name'       => ['required', 'string', 'max:255'],
        'email'      => ['required', 'string', 'email', 'max:255', 'unique:users'],
        'cpf'        => ['required', 'cpf', 'unique:users'],
        'nascimento' => ['required']
    ];
    
    public function list()
    {
        //dd(Auth::user()->influenciadores->firstWhere('rede_social','Instagram'));
        if(Auth::user()->can('admin')) {
            $data = Influenciador::groupBy('user_id')->orderBy('id','desc')->get();
            $array['colunas'] = ['Nome','E-mail'];
            $array['data'] = $data;
            return view('influenciador.lista', ['array'=>$array]);
        } else {
            return view('adm.inicio');
        }
    }
    
    
    public function new()
    {
        return view('influenciador.cadastro');
    }
    
    public function show($id)
    {
        $usuario = User::where([['id',$id]])->first();
        
        return view('influenciador.visualizar', ['usuario' => $usuario]);
    }
    
    public function edit($id)
    {
        $usuario = User::where([['id',$id]])->first();
        
        return view('influenciador.cadastro', ['usuario' => $usuario]);
    }
    
    public function update(Request $request)
    {
        $request->validate([
            'name'       => ['required', 'string', 'max:255'],
            'nascimento' => ['required']
        ]);
        if($request->password != NULL) {
            $request->validate([
                'password' => ['required', 'confirmed', Rules\Password::defaults()],
            ]);
        }
        
        $response = DB::transaction(function() use ($request) {
            $usuario = User::where([['id',$request->id]])->first();
            $array = [
                'name'       => $request->name,
                'nascimento' => $request->nascimento,
            ];
            if($request->password != NULL) { 
                $array += ['password' => Hash::make($request->password)];
            }
            $usuario->update($array);
            $i = 0;
            foreach($request->rede_id as $data) {
                if($i == 1) {
                    $array = $this->getDadosInstagram($request->usuario[$i]);
                }
                if($data != NULL) {
                    Influenciador::where('id',$data)->update([
                        'usuario'     => $request->usuario[$i],
                        'url_foto'    => $array['url_foto'] ?? '',
                        'seguidores'  => $array['seguidores'] ?? 0,
                    ]);
                } elseif($request->usuario[$i] != NULL) {
                    $usuario->influenciadores()->create([
                        'rede_social' => $request->rede[$i],
                        'usuario'     => $request->usuario[$i] ?? '',
                        'url_foto'    => $array['url_foto'] ?? '',
                        'seguidores'  => $array['seguidores'] ?? 0,
                    ]);
                }
                $i++;
            }
            return true;
        });
        if($response == true) {
            return redirect('/influenciador');
        } else {
            return redirect('/influenciador/editar/'.$request->id);
        }
    }
    
    public function store(Request $request)
    {
       
        $request->validate([
            'name'       => ['required', 'string', 'max:255'],
            'email'      => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password'   => ['required', 'confirmed', Rules\Password::defaults()],
            'cpf'        => ['required', 'cpf', 'unique:users'],
            'nascimento' => ['required']
        ]);
        
        $response = DB::transaction(function() use ($request) {
            $usuario = User::create([
                'name'          => $request->name,
                'email'         => $request->email,
                'password'      => Hash::make($request->password),
                'cpf'           => $request->cpf,
                'nascimento'    => $request->nascimento,
            ])->givePermissionTo('influence');
            
            $i = 0;
            foreach ($request->usuario as $data) {
                if($data != NULL) {
                    if($i == 1) {
                        $array = $this->getDadosInstagram($data);
                    }
                    $usuario->influenciadores()->create([
                        'rede_social' => $request->rede[$i],
                        'usuario'     => $data,
                        'url_foto'    => $array['url_foto'] ?? '',
                        'seguidores'  => $array['seguidores'] ?? 0,
                    ]);
                }
                $i++;
            }
            return true;
        });
        if($response == true) {
            return redirect('/influenciador');
        } else {
            return redirect('/influenciador/cadastro');
        }
    }
    
    public function delete(Request $request)
    {
        try {
            DB::beginTransaction();
            Influenciador::where('user_id',$request->id)->delete();
            
            User::where('id', $request->id)->delete();
            DB::commit();
            return Response::json([
                'success' => true,
                'funcao'  => "document.location = '/influenciador'",
                'msg'     => "Influenciador excluído com sucesso.",
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            return Response::json([
                'success' => false,
                'msg'     => "Erro ao excluir o influenciador.".$e,
            ]);
        }
    }
    
    private function getDadosInstagram($usuario)
    {
        $array['seguidores'] = 0;
        $array['url_foto']   = '';
        $array['nome']       = '';
        $response = file_get_contents( "https://www.instagram.com/$usuario/?__a=1");
        //dd($response);
        if($response !== false ) {
            $data = json_decode($response, true);
            if ($data !== null ) {
                $array['nome']       = $data['graphql']['user']['full_name'];
                $array['seguidores'] = $data['graphql']['user']['edge_followed_by']['count'];
                $array['url_foto']   = $data['graphql']['user']['profile_pic_url_hd'];
            }
        }
        return $array;
    }
}
